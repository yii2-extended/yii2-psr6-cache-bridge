<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr6-cache-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2Cache;

use Psr\Cache\CacheItemInterface;
use Stringable;
use yii\caching\CacheInterface;

/**
 * Psr6ToYii2Cache class file.
 * 
 * This class transforms psr-6 compliant cche calls to yii2 compliant cache
 * calls.
 * 
 * @author Anastaszor
 */
class Psr6ToYii2Cache implements \Psr\Cache\CacheItemPoolInterface, Stringable
{
	
	/**
	 * The yii2 compliant cache.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_yii2Cache;
	
	/**
	 * All the deferred items.
	 * 
	 * @var array<integer, CacheItemInterface>
	 */
	protected array $_deferred = [];
	
	/**
	 * Builds a new Psr6Yii2Cache object with the given inner psr-6 cache.
	 * 
	 * @param CacheInterface $cache
	 */
	public function __construct(CacheInterface $cache)
	{
		$this->_yii2Cache = $cache;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::getItem()
	 */
	public function getItem(string $key) : CacheItemInterface
	{
		/** @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value */
		$value = $this->_yii2Cache->get($key);
		
		$item = new Psr6ToYii2CacheItem($key);
		$item->set(false !== $value ? $value : null);
		
		return $item;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::getItems()
	 * @param array<integer|string, string> $keys
	 * @return array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>
	 */
	public function getItems(array $keys = []) : iterable
	{
		$values = [];
		
		$rets = $this->_yii2Cache->multiGet($keys);
		
		foreach($keys as $wantedkey)
		{
			if(isset($rets[$wantedkey]))
			{
				/** @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $ret */
				$ret = $rets[$wantedkey];
				$values[$wantedkey] = $ret;
				continue;
			}
			
			$values[$wantedkey] = new Psr6ToYii2CacheItem($wantedkey);
		}
		
		return $values;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::hasItem()
	 * @param string $key
	 * @return boolean
	 */
	public function hasItem($key) : bool
	{
		return $this->_yii2Cache->exists($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::clear()
	 * @return boolean
	 */
	public function clear() : bool
	{
		$this->_deferred = [];
		
		return $this->_yii2Cache->flush();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::deleteItem()
	 * @param string $key
	 * @return boolean
	 */
	public function deleteItem($key) : bool
	{
		return $this->_yii2Cache->delete($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::deleteItems()
	 * @param array<integer|string, string> $keys
	 * @return boolean
	 */
	public function deleteItems(array $keys) : bool
	{
		$all = true;
		
		foreach($keys as $key)
		{
			$all = $this->deleteItem($key) && $all;
		}
		
		return (bool) $all;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::save()
	 * @return boolean
	 */
	public function save(CacheItemInterface $item) : bool
	{
		$ttl = \method_exists($item, 'getDuration') ? (int) $item->getDuration() : 3600;
		
		return $this->_yii2Cache->set($item->getKey(), $item->get(), $ttl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::saveDeferred()
	 * @return boolean
	 */
	public function saveDeferred(CacheItemInterface $item) : bool
	{
		$this->_deferred[] = $item;
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemPoolInterface::commit()
	 */
	public function commit() : bool
	{
		$all = true;
		
		foreach($this->_deferred as $k => $item)
		{
			$res = $this->save($item);
			if($res)
			{
				unset($this->_deferred[$k]);
			}
			$all = $all && $res;
		}
		
		return (bool) $all;
	}
	
}
