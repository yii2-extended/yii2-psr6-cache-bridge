<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr6-cache-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2Cache;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;
use Stringable;
use yii\caching\Dependency;

/**
 * Yii2ToPsr6Cache class file.
 * 
 * This class transforms yii2 compliant cache calls to psr-6 compliant cache
 * calls.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class Yii2ToPsr6Cache implements \yii\caching\CacheInterface, Stringable
{
	
	/**
	 * The psr-6 compliant cache.
	 * 
	 * @var CacheItemPoolInterface
	 */
	protected CacheItemPoolInterface $_psr6Cache;
	
	/**
	 * Builds a new Yii2ToPsr6Cache object with the given inner yii2 cache.
	 * 
	 * @param CacheItemPoolInterface $cache
	 */
	public function __construct(CacheItemPoolInterface $cache)
	{
		$this->_psr6Cache = $cache;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::buildKey()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @return string
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function buildKey($key) : string
	{
		if(\is_object($key))
		{
			return \md5(\spl_object_hash($key).\serialize($key));
		}
		
		if(\is_array($key))
		{
			return \md5(\serialize($key));
		}
		
		return (string) $key;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::get()
	 * @param null|boolean|integer|float|string|object $key
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 * @psalm-suppress MixedInferredReturnType
	 */
	public function get($key) : mixed
	{
		$item = $this->_psr6Cache->getItem($this->buildKey($key));
		if($item->isHit())
		{
			/** @psalm-suppress MixedReturnStatement */
			return $item->get();
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::exists()
	 * @param null|boolean|integer|float|string|object $key
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function exists($key) : bool
	{
		return $this->_psr6Cache->hasItem($this->buildKey($key));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiGet()
	 * @param array<integer|string, null|boolean|integer|float|string|object> $keys
	 * @return array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>
	 * @throws InvalidArgumentException
	 */
	public function multiGet($keys) : array
	{
		$bkeys = [];
		
		foreach($keys as $key)
		{
			$bkeys[] = $this->buildKey($key);
		}
		
		/** @var array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $hits */
		$hits = [];
		$items = $this->_psr6Cache->getItems($bkeys);
		
		/** @var CacheItemInterface $item */
		foreach($items as $item)
		{
			if($item->isHit())
			{
				/** @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $hit */
				$hit = $item->get();
				$hits[] = $hit;
			}
		}
		
		return $hits;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::set()
	 * @param null|boolean|integer|float|string|object $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param integer $duration
	 * @param ?Dependency $dependency
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function set($key, $value, $duration = null, $dependency = null) : bool
	{
		$item = $this->_psr6Cache->getItem($this->buildKey($key));
		$item->set($value);
		$item->expiresAfter($duration);
		
		return $this->_psr6Cache->save($item);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiSet()
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $items
	 * @param integer $duration
	 * @param ?Dependency $dependency
	 * @return array<integer, string>
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function multiSet($items, $duration = 0, $dependency = null) : array
	{
		$failed = [];
		
		/** @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $item */
		foreach($items as $key => $item)
		{
			$res = $this->set($key, $item, $duration, $dependency);
			if(!$res)
			{
				$failed[] = (string) $key;
			}
		}
		
		return $failed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::add()
	 * @param null|boolean|integer|float|string|object $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param integer $duration
	 * @param ?Dependency $dependency
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function add($key, $value, $duration = 0, $dependency = null) : bool
	{
		if(!$this->exists($key))
		{
			$this->set($key, $value, $duration, $dependency);
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiAdd()
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $items
	 * @param integer $duration
	 * @param ?Dependency $dependency
	 * @return array<integer, string>
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function multiAdd($items, $duration = 0, $dependency = null) : array
	{
		$failed = [];
		
		/** @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $item */
		foreach($items as $key => $item)
		{
			$res = $this->add($key, $item, $duration, $dependency);
			if(!$res)
			{
				$failed[] = (string) $key;
			}
		}
		
		return $failed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::delete()
	 * @param null|boolean|integer|float|string|object $key
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function delete($key) : bool
	{
		return $this->_psr6Cache->deleteItem($this->buildKey($key));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::flush()
	 * @return boolean
	 */
	public function flush() : bool
	{
		return $this->_psr6Cache->clear();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::getOrSet()
	 * @param null|boolean|integer|float|string|object $key
	 * @param callable():(null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>) $callable
	 * @param ?integer $duration
	 * @param ?Dependency $dependency
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function getOrSet($key, $callable, $duration = null, $dependency = null)
	{
		if($this->exists($key))
		{
			return $this->get($key);
		}
		
		$value = $callable();
		if(false !== $value)
		{
			$this->set($key, $value, $duration, $dependency);
		}
		
		return $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetExists()
	 * @param null|boolean|integer|float|string|object $offset
	 * @return boolean
	 */
	public function offsetExists(mixed $offset) : bool
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return false;
		}
		
		try
		{
			return $this->exists($offset);
		}
		catch(InvalidArgumentException $exc)
		{
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetGet()
	 * @param null|boolean|integer|float|string|object $offset
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function offsetGet(mixed $offset) : mixed
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return null;
		}
		
		try
		{
			return $this->get($offset);
		}
		catch(InvalidArgumentException $exc)
		{
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetSet()
	 * @param null|boolean|integer|float|string|object $offset
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 */
	public function offsetSet(mixed $offset, mixed $value) : void
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return;
		}
		
		try
		{
			$this->set($offset, $value);
		}
		catch(InvalidArgumentException $exc)
		{
			return;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetUnset()
	 * @param null|boolean|integer|float|string|object $offset
	 */
	public function offsetUnset(mixed $offset) : void
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return;
		}
		
		try
		{
			$this->delete($offset);
		}
		catch(InvalidArgumentException $exc)
		{
			return;
		}
	}
	
}
