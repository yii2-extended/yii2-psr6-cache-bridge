<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr6-cache-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use yii\caching\CacheInterface;
use Yii2Extended\Yii2Cache\Psr6ToYii2Cache;

/**
 * Psr6ToYii2CacheTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Yii2Cache\Psr6ToYii2Cache
 *
 * @internal
 *
 * @small
 */
class Psr6ToYii2CacheTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Psr6ToYii2Cache
	 */
	protected Psr6ToYii2Cache $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Psr6ToYii2Cache(
			$this->getMockForAbstractClass(CacheInterface::class),
		);
	}
	
}
